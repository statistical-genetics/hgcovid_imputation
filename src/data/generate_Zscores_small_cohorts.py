# coding: utf-8
# Direct imputation with the presence of causal SNPs

import numpy as np
import pandas as pd
import scipy
import os
import sys
import re
from generate_Zscores import *
from sklearn.preprocessing import StandardScaler


if __name__ == '__main__':
    # import sample size each study
    Ncohort = 160
    Ssize = pd.DataFrame({"study" : ["cohort_"+str(i) for i in range(Ncohort)],
    "N_effective":[100]*Ncohort
    })
    Ssize.to_csv("../../data/external/meta_data/N_effective_small_cohort.csv")
    simulate_Z_scores("../../data/processed/Simulated/Genotypes/genotype3.csv", "../../data/processed/Simulated/Genotypes/LD_matrix3.csv", Ssize,"../../data/processed/Simulated/Zscores/Zscore_small_cohort", amp=0.25)
