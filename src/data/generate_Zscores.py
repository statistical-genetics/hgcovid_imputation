# coding: utf-8
# Direct imputation with the presence of causal SNPs

import numpy as np
import pandas as pd
import scipy
import os
import sys
import re
from sklearn.preprocessing import StandardScaler

def exponential_decay(nsnp):
    exp_decay = np.zeros((nsnp, nsnp))
    dec = np.exp(-np.linspace(0,4,nsnp))
    for i in range(nsnp):
        exp_decay[i] = np.roll(dec, i)
    tr = np.triu(exp_decay, k=1)
    tr = tr + np.transpose(tr)
    np.fill_diagonal(tr,1)
    return(tr)

# # # Create a vector of causal effect
# Insert one causal SNPs
def compute_Y(beta, X, sig=1.0):
    noise= np.random.normal(size=X.shape[0], scale=sig)
    Y = X.dot(beta) + noise
    return Y


def compute_Zscore(Y, X, sig):
    nind = X.shape[0]

    Zscores = np.zeros(X.shape[1])
    for i in range(X.shape[1]):
        x = X.iloc[:,i]
        Zscores[i] = x.dot(Y) / (np.linalg.norm(x,2)*sig)
    return Zscores

def generate_null_signal(amp_dummy, LD_cor, X, sig=1.0, sig_genet =0.00001):
    nsnp = LD_cor.shape[0]
    beta = np.random.normal(size=nsnp,scale=sig_genet)
    Y = compute_Y(beta, X, sig)
    Zscore = compute_Zscore(Y, X, sig)
    return({"causal":beta, "Zscore":Zscore,"R2_genet":np.var(X.dot(beta))/np.var(Y)})


def generate_one_causal(amplitude, LD_cor, X, sig=1.0, sig_genet =0.00001):
    nsnp = LD_cor.shape[0]
    beta = np.random.normal(size=nsnp,scale=sig_genet)
    beta[nsnp//2] = amplitude
    Y = compute_Y(beta, X, sig)
    Zscore = compute_Zscore(Y, X, sig)
    return({"causal":beta, "Zscore":Zscore,"R2_genet":np.var(X.dot(beta))/np.var(Y)})

def generate_two_causal(amplitude, LD_cor, X, sig=1.0, sig_genet =0.00001):
    nsnp = LD_cor.shape[0]
    beta = np.random.normal(size=nsnp,scale=sig_genet)
    beta[nsnp//4] = amplitude
    beta[3*(nsnp//4)] = amplitude
    Y = compute_Y(beta, X, sig)

    Zscore = compute_Zscore(Y, X, sig)
    return({"causal":beta, "Zscore":Zscore,"R2_genet":np.var(X.dot(beta))/np.var(Y)})

def generate_two_opposite(amplitude, LD_cor, X, sig=1.0, sig_genet =0.00001):
    nsnp = LD_cor.shape[0]
    beta = np.random.normal(size=nsnp,scale=sig_genet)
    beta[nsnp//4] = amplitude
    beta[3*(nsnp//4)] = -amplitude
    Y = compute_Y(beta, X, sig)
    Zscore = compute_Zscore(Y, X, sig)
    return({"causal":beta, "Zscore":Zscore,"R2_genet":np.var(X.dot(beta))/np.var(Y)})

def define_signal_generator(tag):
    if tag=="null":
        return generate_null_signal
    if tag=="one_causal":
        return generate_one_causal
    if tag=="two_causal":
        return generate_two_causal
    if tag=="two_opposite":
        return generate_two_opposite


def simulate_Z_scores(geno_file, LD_file, cohort_meta,Z_score_path, amp=0.05):
    """
    Parameters :
        geno_file (str): path to the genotype file
        LD_file (str): path to the genotype file
        cohort_meta (pd.DataFrame): Description of the cohort (main point is
         to simulate according to specified sample size). Must contain columns: N_effective, study
        Z_score_path (str) : Path to the output folder + sim prefix to save the results
        amp = amplitude of the causal signal
    """
    # possibles values are null, two_opposite, two_causal, one_causal
    for tag in ["null",'two_opposite', 'two_causal', 'one_causal']:
        print("Simulating values for {}".format(tag))
        #get genotype
        X = pd.read_csv(geno_file, sep="\t")
        LD_cor = pd.read_csv(LD_file, sep="\t")
        LD_cor = LD_cor.values
        nsnp = X.shape[1]

        scaler = StandardScaler()
        # transform to centered scaled
        X = pd.DataFrame(scaler.fit_transform(X))
        #LD_cor = X.corr().values
        n_snp = LD_cor.shape[0]

        print('search of best rd for amplitude {}'.format(amp))
        signal_generator = define_signal_generator(tag)
        signal = signal_generator(amp, LD_cor, X, sig=1)

        Zscore = signal["Zscore"]
        beta = signal["causal"]

        All_Zscore = pd.DataFrame({"Beta_all_SNPs" : Zscore / X.shape[0]**0.5, "Zscore_all_SNPs" : Zscore})

        for i, neff in enumerate(cohort_meta.N_effective):
            print(i, neff)

            id_start = cohort_meta.N_effective[:i].sum()
            id_end = cohort_meta.N_effective[:(i+1)].sum()

            study = cohort_meta.study.iloc[i]
            print("Number of sample for {}".format(study))
            print(cohort_meta.N_effective.iloc[i])

            X_study = X.iloc[id_start:id_end,]

            signal = signal_generator(amp, LD_cor, X_study, sig=1)
            Zscore = signal["Zscore"]
            print("R2_genet {}".format(signal["R2_genet"]))
            All_Zscore["Z_{0}".format(study)]  = Zscore
            All_Zscore["Beta_{0}".format(study)]  = Zscore / X_study.shape[0]**0.5

        All_Zscore.to_csv("{0}_{1}.csv".format(Z_score_path, tag))


if __name__ == '__main__':
    # import sample size each study

    Ssize = pd.read_csv("../../data/external/meta_data/Ncases_B2_vs_ALL.tsv", sep="\t")
    Ssize = Ssize.loc[[re.search("EUR",i)!=None for i in Ssize.Name],]

    Ssize["per"] = Ssize.n_cases / (Ssize.n_cases + Ssize.n_controls)
    Ssize["study"] = Ssize.Name
    Ssize["N_effective"] = np.floor(Ssize.per * (1- Ssize.per) * (Ssize.n_cases + Ssize.n_controls))
    Ssize.N_effective = Ssize.N_effective.astype(int, copy=False)
    Ssize.to_csv("../../data/external/meta_data/N_effective.csv")
    print(Ssize.head())
    simulate_Z_scores("../../data/processed/Simulated/Genotypes/genotype3.csv", "../../data/processed/Simulated/Genotypes/LD_matrix3.csv", Ssize,"../../data/processed/Simulated/Zscores/Zscore", amp=0.25)
