import pandas as pd
import re

D = pd.read_csv("/mnt/zeus/GGS/PROJECT_imputation_covidhg/hgcovid_imputation/data/interim/Zscores_hg37/zscore_hg37.csv", sep="\t")

D.loc[D['#CHR']==19].head()
other_data = ['#CHR', 'POS', 'REF', 'ALT', 'SNP','rsid', 'loc',
       'hg37_pos']
eur_study = [n for n in D.columns if re.search("EUR", n)]
study_key = pd.Series([re.match(r"(\w+)_EUR_",i).group(1) for i in eur_study]).unique()

for std in study_key:
    D[std+"_EUR_Z"] = D[std+"_EUR_beta"] / D[std+"_EUR_sebeta"]
eur_study = [n for n in D.columns if re.search("EUR", n)]


filled_out_study = (~D[eur_study].isna()).mean(0).sort_values(ascending=False)
filled_out_study = filled_out_study.loc[filled_out_study > 0.8]
complete_snp = (~D[filled_out_study.index].isna()).all(1)


D['SNP_hg37'] = D["#CHR"].astype(str) + ":"+D.hg37_pos.astype(str)+":"+ D.REF+":"+D.ALT

all_col =other_data +["SNP_hg37"]+ list(filled_out_study.index)


D.loc[complete_snp, all_col].to_csv("/mnt/zeus/GGS/PROJECT_imputation_covidhg/hgcovid_imputation/data/interim/Zscores_hg37/Z_scores_EUR.tsv", sep="\t")
