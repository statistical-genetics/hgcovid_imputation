# coding: utf-8
# Direct imputation with the presence of causal SNPs

import numpy as np
import pandas as pd
import raiss
import sys

def get_perf(rd, Zscore, LD_cor, ids_known, ids_masked):
    imputed = raiss.stat_models.raiss_model(Zscore[ids_known], pd.DataFrame(LD_cor[ids_known,:][:,ids_known]), LD_cor[ids_masked,:][:,ids_known], rcond=rd)
    reconstructed = imputed['mu']* np.sqrt(1-imputed['var'])
    cor_perf = np.corrcoef(reconstructed, Zscore[ids_masked])[0,1]
    MAE = np.linalg.norm(reconstructed - Zscore[ids_masked], 1) / len(ids_masked) # L1- loss

    id_large = np.where(np.abs(Zscore[ids_masked]) > 1)[0]
    cor_large = np.corrcoef(reconstructed[id_large], Zscore[ids_masked][id_large])[0,1]
    MAE_large = np.linalg.norm(reconstructed[id_large] - Zscore[ids_masked][id_large], 1) / len(id_large)
    return({'cor':cor_perf, "MAE": MAE, "cor_large":cor_large, "MAE_large":MAE_large})

# Get best rd
def best_rd(zscore, LD_cor, ids_known, ids_masked):
    rd_list = np.linspace(0.001,0.5, 8)
    cor_perf = pd.Series(index=rd_list)
    MAE_perf = pd.Series(index=rd_list)
    cor_large = pd.Series(index=rd_list)
    MAE_large = pd.Series(index=rd_list)
    for rd in pd.Index(rd_list):
        perf = get_perf(rd, zscore, LD_cor, ids_known, ids_masked)
        cor_perf[rd] = perf["cor"]
        MAE_perf[rd] = perf["MAE"]
        cor_large[rd] = perf["cor_large"]
        MAE_large[rd] = perf["MAE_large"]
        #print(rd)
        #print(cor_perf[rd])
    print("best rd")
    best_rd = rd_list[np.nanargmax(cor_perf)]
    print(best_rd)
    print(cor_perf[best_rd])
    return({"correlation": cor_perf[best_rd],
            "MAE": MAE_perf[best_rd],
            "correlation_large": cor_large[best_rd],
            "MAE_large": MAE_large[best_rd],
            "rcond":best_rd})


if __name__ == '__main__':
    # import sample size each study
    print(sys.argv)
    meta_data_file = sys.argv[1]
    Zscore_prefix = sys.argv[2]
    Imputed_prefix = sys.argv[3]

    LD_cor = pd.read_csv("../../data/processed/Simulated/Genotypes/LD_matrix3.csv", sep="\t", index_col=0)
    LD_cor = LD_cor.values
    print(LD_cor.shape)
    Ssize = pd.read_csv("{0}".format(meta_data_file),sep=",", index_col=1 )
    print(Ssize.head())

    for tag in ["null","one_causal", 'two_opposite', 'two_causal']:
        Zscores = pd.read_csv("../../data/processed/Simulated/Zscores/Zscore_{0}{1}.csv".format(Zscore_prefix,tag), sep=",", index_col=0)

        n_masked = 25
        n_snp= LD_cor.shape[0]
        print(n_snp)
        ids_masked = np.random.choice(np.arange(0,99, 1), n_masked, replace=False)
        ids_known = np.setdiff1d(np.array(range(n_snp)), ids_masked)

        opt_rs = best_rd(Zscores["Zscore_all_SNPs"].values, LD_cor, ids_known, ids_masked)
        print(Zscores["Zscore_all_SNPs"].head())

        Imputated_Zscore = pd.DataFrame({"Z_ref" :Zscores["Zscore_all_SNPs"][ids_masked]/ (16000**0.5),
                                        "Beta_ref" :Zscores["Beta_all_SNPs"][ids_masked]})

        # Imputation based on reference

        Zs  = raiss.stat_models.raiss_model(Zscores["Zscore_all_SNPs"][ids_known], pd.DataFrame(LD_cor[ids_known,:][:,ids_known]), LD_cor[ids_masked,:][:,ids_known], rcond=opt_rs["rcond"])
        Imputated_Zscore["Beta_imp_from_ref"]  = Zs["mu"] / 16000**0.5
        Imputated_Zscore["Z_imp_from_ref"]  = Zs["mu"]


        for study in [i for i in Zscores.columns[2:] if i[:2]=="Z_"]:
            print(study)
            print(Ssize.loc["{0}".format(study[2:])])
            Zscore = Zscores[study]
            Zs  = raiss.stat_models.raiss_model(Zscore[ids_known], pd.DataFrame(LD_cor[ids_known,:][:,ids_known]), LD_cor[ids_masked,:][:,ids_known], rcond=opt_rs["rcond"])

            Imputated_Zscore["Beta_{0}".format(study[2:])]  = Zs["mu"] / Ssize.N_effective.loc["{0}".format(study[2:])]**0.5
            Imputated_Zscore[study]  = Zs["mu"]
            Imputated_Zscore["var_{0}".format(study[2:])]  = Zs["var"]

        Imputated_Zscore.to_csv("../../data/processed/Simulated/Imputed/Imputed_{0}{1}.csv".format(Imputed_prefix, tag))
