Hgcovid_imputation
==============================

Simulations study to assess the effect of imputation error in meta-analysis

Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          
    │
    ├── reports  
    │

    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           
    │   │   └
    |       |__ 1 generate_genotype.R : enerate genotype with a specified LD structure
    |       |__ 2 generate_Zscores.py
    |       |    Generate estimated Zscore by simulating a continuous
                 phenotypes from genotype and  
            |     normal noise. then estimate beta and Zscore with a linear regression. Cohort size
            |   are generated based on hgcovid cohort sizes
            | _ 3 generate_Zscores_small_cohort.py
            |    Variant of the precedent script to simulate Z-scores using
            | only small cohorts. Hypothesis to test:
            |    Can we recover a proper signal in the meta analysis using
            | very noisy input
            |_  4 retrieve_european_cohort.py : Filter to retrieve european study with enough SNP available

    │   │
    │   ├── features       
    │   │   └── add_hg_37_pos.py : perform liftover to hg37 (ad GnomAD is in hg37)
    |   |   |__ retrieve_filled_out.py : Retrieve european studies with more than 80% of filled out SNPS
    │   │
    │   ├── models         
    │   │   │   
    │   │   ├── Impute_simulated_signal.py
    |           impute Zscores using simulated data : mask 50 SNPs on  
                200,     reimpute then save results
    │   │   └── Imputation_strategy_simulation.py
                impute Zscores using simulated data : mask 50 SNPs on  
                200,     reimpute then save results assuming different sample size (based on hgcovid consortium or always 100)
            |__ Impution_real_data.py
            |__ Imputation_test_real_data.py : mask 10% of SNPs around significant loci and impute them back
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── Draw_LD.R : Draw the LD matrix used to simulate Data
    |       |__ Draw_Imputation_quality.R : draw non imputed signal
    |           (intrinsic variability due to sample size)
    |       |__ Draw_signal_variability.R : draw imputed signal
    |           ( variability due to sample size + imputation error)
            |_ Imputation_strategy_real_data.R : Compare meta_analysis based on original Z-scores vs Imputed Z-scores
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
